﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

	public AudioClip jumping;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		var fox = GetComponent<FoxAgent>();
		var audioSource = GetComponent<AudioSource>();

		if (fox.IsWalking)
		{
			if (!audioSource.isPlaying)
			{
				audioSource.Play();
			}
		}
		else if (fox.Jumped)
		{
			audioSource.Stop();
			audioSource.PlayOneShot(jumping);
		}
		else if (!fox.IsJumping)
		{
			audioSource.Stop();
		}
	}
}
