﻿using UnityEngine;
using System.Collections;

public class PlayerRespawnHandler : MonoBehaviour
{
	private Vector3 initialPosition;

	void Start ()
	{
		initialPosition = transform.position;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		var enemy = collision.gameObject.GetComponent<Enemy>();
		if (enemy != null)
		{
			Respawn();
		}
	}

	private void Respawn()
	{
		transform.position = initialPosition;
	}
}
