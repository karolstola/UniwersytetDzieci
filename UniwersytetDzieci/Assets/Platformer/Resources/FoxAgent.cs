﻿using UnityEngine;
using System.Collections;

public class FoxAgent : CreatureGameAgent
{
	private enum MovingDirection
	{
		LEFT,
		RIGHT,
		NONE
	}

	public float floorCheckOffset;
	private float floorCheckRadius = 0.1f;
	public float floorCheckWidth = 1f;
	private MovingDirection movingDirection = MovingDirection.NONE;

    int moveState = 0;
    bool facingRight = true;

    const int MOVE_STATE_IDLE = 0;
    const int MOVE_STATE_JUMP = 2;
    const int MOVE_STATE_POST_JUMP = 3;

	bool left_down;
	bool right_down;
	bool left_up ;
	bool right_up ;
	bool space_down ;
	bool stoppedTouchingGround = false;
	float horizontalInput;
	bool jumped = false;

	public bool IsWalking
	{
		get
		{
			return horizontalInput != 0 && IsTouchingGround();
		}
	}

	public bool Jumped
	{
		get
		{
			return jumped;
		}
	}

	public bool IsJumping
	{
		get
		{
			return moveState == MOVE_STATE_POST_JUMP;
		}
	}

	public FoxAgent()
        : base()
    {
        moveState = MOVE_STATE_IDLE;
    }

    //public override void updateStep()
	public void Update()
    {
        base.updateStep();
		jumped = false;
        left_down = Input.GetKeyDown(KeyCode.A);
        right_down = Input.GetKeyDown(KeyCode.D);
        left_up = Input.GetKeyUp(KeyCode.A);
        right_up = Input.GetKeyUp(KeyCode.D);
        space_down = Input.GetKeyDown(KeyCode.Space);

		horizontalInput = Input.GetAxis("Horizontal");
		if (horizontalInput < 0)
        {
			facingRight = false;
			movingDirection = MovingDirection.LEFT;
        }
        else if (horizontalInput > 0)
        {
			facingRight = true;
			movingDirection = MovingDirection.RIGHT;
        }
		else
		{
			movingDirection = MovingDirection.NONE;
		}

        if (space_down)
        {
            moveState = MOVE_STATE_JUMP;

        }

        setMovement();
        setAnimation();
    }

    private void setMovement()
    {
        var creature_renderer = game_controller.creature_renderer;
        var parent_obj = creature_renderer.gameObject;
        Rigidbody2D parent_rbd = parent_obj.GetComponent<Rigidbody2D>();
        var speed = 25.0f;
        var curVel = parent_rbd.velocity;

        if (movingDirection == MovingDirection.LEFT)
        {
			parent_rbd.velocity = new Vector3(-speed, curVel.y, 0);
        }
        else if (movingDirection == MovingDirection.RIGHT)
        {
            parent_rbd.velocity = new Vector3(speed, curVel.y, 0);
        }
        else if (moveState == MOVE_STATE_IDLE)
        {
            parent_rbd.velocity = new Vector3(0, curVel.y, 0);
        }

		if (moveState == MOVE_STATE_JUMP)
        {
			if(IsTouchingGround())
			{
				var jumpForceX = 0f;
				if (movingDirection == MovingDirection.LEFT)
				{
                   jumpForceX = 30f;
                }
				else if(movingDirection == MovingDirection.RIGHT)
				{
					jumpForceX = -30f;
				}
                parent_rbd.AddForce(new Vector3(jumpForceX, 150, 0), ForceMode2D.Impulse);
				jumped = true;
			}
            moveState = MOVE_STATE_POST_JUMP;
        }
        else if (moveState == MOVE_STATE_POST_JUMP)
        {
			if(stoppedTouchingGround)
			{
				if (IsTouchingGround())
				{
					stoppedTouchingGround = false;
					moveState = MOVE_STATE_IDLE;
				}
			}
			else
			{
				if(!IsTouchingGround())
				{
					stoppedTouchingGround = true;
				}
			}
        }
    }

    private void setAnimation()
    {
        var creature_renderer = game_controller.creature_renderer;
        var local_size = 0.2f;
        creature_renderer.blend_rate = 0.2f;

        if (facingRight)
        {
            creature_renderer.transform.localScale = new Vector3(-local_size, local_size, 1);
        }
        else
        {
            creature_renderer.transform.localScale = new Vector3(local_size, local_size, 1);
        }

        
        if ((moveState == MOVE_STATE_JUMP)
            || (moveState == MOVE_STATE_POST_JUMP))
        {
            creature_renderer.BlendToAnimation("jump");
        }
		else if (movingDirection != MovingDirection.NONE)
		{
			creature_renderer.BlendToAnimation("run");
		}
		else if (moveState == MOVE_STATE_IDLE)
        {
            creature_renderer.creature_manager.ResetBlendTime("jump");
            creature_renderer.BlendToAnimation("default");
        }
    }

    private void turnOffOtherObjectCollisions(GameObject[] objectList)
    {
        var creature_renderer = game_controller.creature_renderer;
        var parent_obj = creature_renderer.gameObject;
        var self_collider = parent_obj.GetComponent<Collider2D>();

        foreach (var curObject in objectList)
        {
            var object_collider = curObject.GetComponent<Collider2D>();
            Physics2D.IgnoreCollision(self_collider, object_collider);
        }
    }

    public override void initState()
    {
        base.initState();

        //turnOffOtherObjectCollisions(GameObject.FindGameObjectsWithTag("horseman_tag"));
        //turnOffOtherObjectCollisions(GameObject.FindGameObjectsWithTag("bat_tag"));
    }

	public void OnDrawGizmos()
	{
		
		Gizmos.DrawWireCube(GetFloorCheckPosition(), GetFloorCheckSize());
	}

	private Vector2 GetFloorCheckPosition()
	{
		var center = game_controller.creature_renderer.transform.position;
		center.y += floorCheckOffset;
		return center;
	}

	private Vector2 GetFloorCheckSize()
	{
		return new Vector2(floorCheckWidth, floorCheckRadius);
	}

	private bool IsTouchingGround()
	{
		var groundObjs = Physics2D.OverlapBoxAll(GetFloorCheckPosition(), GetFloorCheckSize(), 0f);
		foreach (var ground in groundObjs)
		{
			if (ground.tag == "ground_tag")
			{
				return true;
			}
		}
		return false;
	}
}